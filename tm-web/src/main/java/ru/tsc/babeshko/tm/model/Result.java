package ru.tsc.babeshko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Result {

    @NotNull
    private Boolean success = true;

    @NotNull
    private String message = "";

    public Result(@NotNull final Boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }

}