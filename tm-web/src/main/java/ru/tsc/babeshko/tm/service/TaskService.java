package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyNameException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.repository.TaskRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    public Task add(@Nullable final String name, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task(name);
        task.setUserId(userId);
        return taskRepository.save(task);
    }

    public Task save(@Nullable final Task task, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new TaskNotFoundException();
        task.setUserId(userId);
        return taskRepository.save(task);
    }

    @Transactional
    public void remove(@Nullable final Task task, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new TaskNotFoundException();
        removeByIdAndUserId(task.getId(), userId);
    }

    @Transactional
    public void remove(@Nullable final List<Task> tasks, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (tasks == null) throw new TaskNotFoundException();
        tasks.forEach(task -> remove(task, userId));
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteByIdAndUserId(id, userId);
    }

    @NotNull
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAllByUserId(userId);
    }

    @Nullable
    public Task findByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findByIdAndUserId(id, userId).orElse(null);
    }

    public boolean existsByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.existsByIdAndUserId(id, userId);
    }

    @Transactional
    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteAllByUserId(userId);
    }

    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.countByUserId(userId);
    }

}