package ru.tsc.babeshko.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
@Setter
public class CustomUser extends org.springframework.security.core.userdetails.User {

    @Nullable
    private String userId = null;

    public CustomUser(@NotNull final org.springframework.security.core.userdetails.User user) {
        super(
                user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities()
        );
    }

    public CustomUser(@NotNull final org.springframework.security.core.userdetails.User user, @NotNull final User userModel) {
        super(
                user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities()
        );
        userId = userModel.getId();
    }

    public CustomUser(
            String userName,
            String password,
            Collection<? extends GrantedAuthority> authorities
    ) {
        super(userName, password, authorities);
    }

    public CustomUser(
            String userName,
            String password,
            boolean enabled,
            boolean accountNonExpired,
            boolean credentialsNonExpired,
            boolean accountNonLocked,
            Collection<? extends GrantedAuthority> authorities
    ) {
        super(userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

}