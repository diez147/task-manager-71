package ru.tsc.babeshko.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tsc.babeshko.tm.model.Result;
import ru.tsc.babeshko.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface AuthEndpoint {

    @NotNull
    @WebMethod
    @PostMapping(value = "/login")
    Result login(@NotNull @WebParam(name = "username") String username,
                 @NotNull @WebParam(name = "password") String password
    );

    @NotNull
    @WebMethod
    @GetMapping(value = "/profile")
    User profile();

    @NotNull
    @WebMethod
    @PostMapping(value = "/logout")
    Result logout();

}
