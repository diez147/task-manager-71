package ru.tsc.babeshko.tm.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.babeshko.tm.configuration.ApplicationConfiguration;
import ru.tsc.babeshko.tm.marker.UnitCategory;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.service.ProjectService;
import ru.tsc.babeshko.tm.service.UserService;
import ru.tsc.babeshko.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectControllerTest {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    private final Project project = new Project("test");

    @Nullable
    private static String USER_ID;

    @Before
    public void init() {
        userService.createUser("test", "test", null);
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
    }

    @After
    public void destroy() {
        projectService.clearByUserId(USER_ID);
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(1, projectService.findAllByUserId(USER_ID).size());
    }

    @Test
    @SneakyThrows
    public void delete() {
        projectService.save(project, USER_ID);
        Assert.assertEquals(1, projectService.findAllByUserId(USER_ID).size());
        @NotNull final String url = "/project/delete/" + project.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(0, projectService.findAllByUserId(USER_ID).size());
    }

    @Test
    @SneakyThrows
    public void edit() {
        @NotNull final String url = "/project/edit/" + project.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    public void editPost() {
        @NotNull final String url = "/project/edit/" + project.getId();
        @NotNull final String json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}