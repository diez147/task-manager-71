package ru.tsc.babeshko.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.dto.request.DataBackupSaveRequest;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.event.ConsoleEvent;

@Component
public final class DataBackupSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "backup-save";

    @NotNull
    public static final String DESCRIPTION = "Save backup to json file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBackupSaveListener.getName() == #event.name")
    public void handler(final ConsoleEvent event) {
        getDomainEndpoint().saveDataBackup(new DataBackupSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}