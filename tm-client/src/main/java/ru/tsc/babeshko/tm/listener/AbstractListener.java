package ru.tsc.babeshko.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.api.model.ICommand;
import ru.tsc.babeshko.tm.api.service.ITokenService;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
public abstract class AbstractListener implements ICommand {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    public abstract void handler(@NotNull final ConsoleEvent event);

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@NotNull final String token) {
        tokenService.setToken(token);
    }

    @Override
    public String toString() {
        String result = "";
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}